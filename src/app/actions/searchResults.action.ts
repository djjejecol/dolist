import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { IVenue } from '../models/IVenue';

// True while fetching data from API
export const LOADING = 'Venue Load';

// Searching venue via venue Search API
export const SEARCH = 'Venue Search';
export const SEARCH_DONE = 'Venue Search Done';

export class Search implements Action {
  readonly type = SEARCH;
  constructor(public payload: string) { }
}

export class SearchDone implements Action {
  readonly type = SEARCH_DONE;
  constructor(public payload: IVenue[]) { }
}

export type Actions = Search | SearchDone;
