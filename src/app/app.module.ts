import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment'; // Angular CLI environemnt
import { SearchVenuesEffects } from './effects/searchVenues.effect';
import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { reducer } from './reducers/searchResults.reducer';
import { SearchVenuesService } from './services/searchVenues.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    UiModule,
    HttpClientModule,
    StoreModule.forRoot({
      venues: reducer
    }),
    EffectsModule.forRoot([SearchVenuesEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
  ],
  providers: [SearchVenuesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
