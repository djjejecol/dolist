import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @Input() title = 'title';
  @Input() placeholder = 'placeholder';
  @Output() search: EventEmitter<any> = new EventEmitter();

  value = '';


  constructor() { }

  onClickSearch(value: string) {
    this.submitSearchValue(value);
  }

  onEnter(value: string) {
    this.submitSearchValue(value);
  }

  submitSearchValue(value) {
    this.value = value;
    this.search.emit(this.value);
  }

  ngOnInit() {
  }

}
