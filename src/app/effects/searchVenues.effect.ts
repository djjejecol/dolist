import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as SearchActions from '../actions/searchResults.action';
import { SearchVenuesService } from '../services/searchVenues.service';

import 'rxjs/add/operator/mergeMap';

@Injectable()
export class SearchVenuesEffects {

constructor(private actions$: Actions,
            private service: SearchVenuesService) {}

  @Effect()
  searchVenues$: Observable<Action> = this.actions$
  // Listen for the 'SEARCH' action
  .ofType<SearchActions.Search>(SearchActions.SEARCH)
  .mergeMap(query => {
      return this.service.searchVenues(query.payload)
      .map(results => new SearchActions.SearchDone(results));
  });
}
