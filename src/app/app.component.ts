import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './app.state';
import { Search } from './actions/searchResults.action';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'doList';
  loading$: any;
  results$: any;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.loading$ = this.store.select(state => state.venues.loading);
    this.results$ = this.store.select(state => state.venues.results);
  }
  onSearch(value) {
    this.store.dispatch(new Search(value));
  }
}
