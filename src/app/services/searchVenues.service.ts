import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { IVenue } from '../models/IVenue';

@Injectable()
export class SearchVenuesService {

  constructor(private http: HttpClient) {
  }

  searchVenues(query: string): Observable<IVenue[]> {
    const { endpoint, client, service, version } = environment.api;
    const url = `${endpoint}${service}/search?`;
    const params: string = [
      `near=${query}`,
      `client_id=${client.id}`,
      `client_secret=${client.secret}`,
      `v=${version}`,
    ].join('&');

    const queryUrl = `${url}${params}`;

    return this.http.get(queryUrl).map((data: any) => {
      return data && data.response && data.response.venues || [];
    });
  }
}