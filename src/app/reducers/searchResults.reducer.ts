import { IVenue } from '../models/IVenue';

// importing Actions
import * as SearchActions from '../actions/searchResults.action';

export interface State {
    loading: boolean;
    searchTerm: string;
    results: IVenue[];
}

const initialState: State =  {
    loading: false,
    searchTerm: '',
    results: [],
};

const reduce = {
    [SearchActions.SEARCH]: (state, payload) => ({
        ...state,
        loading: true,
    }),
    [SearchActions.SEARCH_DONE]: (state, payload) => ({
        ...state,
        loading: false,
        results: [...payload]
    }),
};

export function reducer(state = initialState, action: SearchActions.Actions): State {
    console.log(action);
    return reduce.hasOwnProperty(action.type) ?
        reduce[action.type](state, action.payload) :
        state;
}
