import * as fromSearchResult from './reducers/searchResults.reducer';

export interface AppState {
  readonly venues: fromSearchResult.State;
}
