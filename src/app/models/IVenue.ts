export interface IVenue {
  id: string;
  name: string;
  location: ILocation;

}
