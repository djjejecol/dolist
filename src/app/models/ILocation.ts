interface ILocation {
  address: string;
  postalCode: string;
  city: string;
  country: string;
  formattedAddress: string[];
}